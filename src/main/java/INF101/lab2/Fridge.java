package INF101.lab2;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
 
    public List<FridgeItem> fridgelist = new ArrayList<FridgeItem>();

    
    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        int size = fridgelist.size();
        return size;
    }
    
    int cap = 20;
    @Override
    public int totalSize() {
        // TODO Auto-generated method stub
        return cap;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // TODO Auto-generated method stub
        if(nItemsInFridge() == cap){
            return false;
        }
        else{
        fridgelist.add(item);
        }
        return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if(nItemsInFridge() == 0) {
            throw new NoSuchElementException();
        }else{
        
        fridgelist.remove(item);
        } 
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
        fridgelist.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        List<FridgeItem> expired = new ArrayList<>();
        for(FridgeItem item : fridgelist){
            if(item.hasExpired()){
                expired.add(item);
            }
                
        }
        for(FridgeItem expiredItem : expired){
            fridgelist.remove(expiredItem);
        }
            

        
        return expired;
    }
    
}
